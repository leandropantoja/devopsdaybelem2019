package com.vibe.devopsday.pratostipicos.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vibe.devopsday.pratostipicos.model.PratoTipico;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/pratosTipicos")
public class PratosTipicosController {

	@RequestMapping(value = "/",produces = "text/html")
    public String home() {
        return "<h1>Pratos Típicos do Pará</h1><p><h2>Cardápio</h2></p><p>Clique <a href=\"/pratosTipicos/todos\">aqui</a> para ver a " +
            "lista de pratos</p>";
    }
    
    @RequestMapping(value = "/todos", produces = "application/json")
    public List<PratoTipico> getAll(){
    	
    	List<PratoTipico> pratosTipicos  = new ArrayList<PratoTipico>();
    	
    	pratosTipicos.add(new PratoTipico(1l, "Maniçoba", "Descrição Maniçoba", 15,"manicoba.png"));
    	pratosTipicos.add(new PratoTipico(1l, "Tacacá", "Descrição Tacacá", 12,"tacaca.png"));
    	pratosTipicos.add(new PratoTipico(1l, "Pato no Tucupí", "Descrição Pato no Tucupi", 20,"pato.png"));
    	pratosTipicos.add(new PratoTipico(1l, "Caldeirada", "Caldeirada", 45,"caldeirada.png"));
    	pratosTipicos.add(new PratoTipico(1l, "Bolo", "Descrição Bolo Podre", 8, "bolo.png"));
    	pratosTipicos.add(new PratoTipico(1l, "Macaxeira", "Bolo de Macaxeira", 7,"macaxeira.png"));
    	pratosTipicos.add(new PratoTipico(1l, "Açai", "Descrição Açai", 16,"acai.png"));
    	
    	return pratosTipicos;
    }
    
    @RequestMapping(value = "/isAlive",produces = "application/json")
    public String isAlive() {
        return "ok";
    }
    
    
    @RequestMapping(value = "/versao",produces = "application/json")
    public String apiVersion( ) throws FileNotFoundException, IOException, XmlPullParserException{
    	
    	MavenXpp3Reader reader = new MavenXpp3Reader();
        org.apache.maven.model.Model model = reader.read(new FileReader("pom.xml"));
    	
        String version = model.getVersion();
        
        return version;
    }
    
}
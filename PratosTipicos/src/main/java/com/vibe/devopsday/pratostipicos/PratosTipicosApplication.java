package com.vibe.devopsday.pratostipicos;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PratosTipicosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PratosTipicosApplication.class, args);
	}
}

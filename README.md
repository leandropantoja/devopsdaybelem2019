# devOpsDayBelem2019


1 - Tradicional:

Deploy da versão 1

kubectl apply -f pratos-tipicos-deployment.yaml -n nginx-ingress

kubectl apply -f pratos-tipicos-service.yaml -n nginx-ingress

Deploy da versão 2 (Com o watch para monitorar)

kubectl apply -f pratos-tipicos-deployment-v2.yaml -n nginx-ingress


Deleção:

kubectl delete -f . -n nginx-ingress


2 - Rollout:


Deploy da versão 1

kubectl apply -f pratos-tipicos-deployment.yaml -n nginx-ingress

kubectl apply -f pratos-tipicos-service.yaml -n nginx-ingress

Deploy da versão 2 (Com o watch para monitorar)

kubectl apply -f pratos-tipicos-deployment-v2.yaml -n nginx-ingress


Deleção:

kubectl delete -f . -n nginx-ingress


2 - Blue-Green:

kubectl apply -f . -n nginx-ingress

Direcionamento:

kubectl apply -f direcionar-para-green.yaml -n nginx-ingress

kubectl apply -f direcionar-para-blue.yaml -n nginx-ingress


Deleção:

kubectl delete -f . -n nginx-ingress


3 - Canary

kubectl apply -f pratos-tipicos-deployment.yaml -n nginx-ingress

kubectl apply -f pratos-tipicos-service.yaml -n nginx-ingress


Deploy da versão 2 (Com o watch para monitorar)

kubectl apply -f pratos-tipicos-deployment-v2.yaml -n nginx-ingress


Escalonamento:

kubectl scale --replicas=4 deployment pratos-tipicos-v2 -n nginx-ingress

kubectl scale --replicas=0 deployment pratos-tipicos-v1 -n nginx-ingress

